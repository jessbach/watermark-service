package controllers

import play.api.libs.json._
import play.api.mvc._
import models._
import play.api.mvc._
import play.api.libs.iteratee.{Iteratee, Concurrent, Enumerator}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import play.api.Logger
import play.api.libs.concurrent.{Akka, Promise}
import java.io.File
import akka.actor.{Props, Actor}
import akka.pattern.ask
import java.util.concurrent.TimeoutException
import play.api.Play.current
import service._

object Application extends Controller {

	/**
	 * Creates a new ticket for a posted document and starts watermarking service in async mode.
	 * @param request.body - Should be the corresponding document encoded in json
	 * 						 (example: {"author": {"firstname" : "Paul", "lastname" : "Chiusano"}, "title": "Functional Programming in Scala", "topic": "Business"})
	 *        				 If topic is not set, the document will be an Journal else Book.
	 * @POST
	 */			
	def createDocument = Action(BodyParsers.parse.json) { request =>
		val receivedDocument: JsResult[Document] = (request.body).validate[Document]
			receivedDocument match {
			case s: JsSuccess[Document] => {
				val newTicket = new Ticket(s.get)
				val ticketId = newTicket.key
				WatermarkService.generateWatermark(newTicket)
				Ok(Json.obj("status" -> "ok", "message" -> s"Ticket with Id $ticketId created", "ticket" -> Json.toJson(newTicket)))
			}
			case e: JsError => {
				BadRequest(Json.obj("status" -> "error", "message" -> JsError.toFlatJson(e)))
			}
		}
	}

	/**
	 * Websocket Ressource <ws://127.0.0.1:9000/websocket>
	 * 
	 * Sending a document (example: {"author": {"firstname" : "Paul", "lastname" : "Chiusano"}, "title": "Functional Programming in Scala", "topic": "Business"})
	 * gives you direct bidiretional information about state.
	 * 
	 * @Note https://github.com/fdimuccio/play2-sockjs/issues/11
	 */
	def simpleWebSockdet = WebSocket.using[JsValue] { request =>
		val (out, channel) = Concurrent.broadcast[JsValue]
		WatermarkService.listen(new Event[JsValue] {
			def event(ev: JsValue) {
				channel.push(ev)  
			}
		})
		val in = Iteratee.foreach[JsValue] { msg => {
			val receivedDocument: JsResult[Document] = (msg).validate[Document]
			receivedDocument match {
				case s: JsSuccess[Document] => {	
					WatermarkService.generateWatermark(new Ticket(s.get))
				}
				case e: JsError => {
					channel.push(Json.obj("status" -> "error", "message" -> JsError.toFlatJson(e)))
				}
			}
			}
		}
		(in, out)
	}
		
	/**
	 * Receives an document for a given Ticket if available. If ticketId could
	 * not be found an BadRequest with "Could not find ticket" is send back.
	 * 
	 * @GET
	 */
	def getDocument(ticketId: Long) = Action {
		queryTicket(ticketId, (t: Ticket) => {
			Ok(Json.toJson(t.document))
		})
	}

	/**
	 * Deletes an ticket if ticketId could be found, else sends an BadRequest
	 * with "Could not find ticket" back.
	 * 
	 * @GET
	 */
	def deleteDocument(ticketId: Long) = Action {
		queryTicket(ticketId, (t: Ticket) => {
			TicketStorage.delete(ticketId)
			Ok(s"Ticket with id $ticketId deleted")
		})
	}
  
	/**
	 * Checks the state of the ticket with given id. 
	 * Meaning if watermarking the document is in "Init", "Processing" or "Done" state.
	 * 
	 * @HEAD
	 */
	def checkTicket(ticketId: Long) = Action {
		queryTicket(ticketId, (t: Ticket) => Ok(t.status.toString))
	}
	
	/**
	 * Checks ticket in storage and applies a function to the ticket. (Convenience Method)
	 * If successful the new ticket is returned, else an bad request is send back.
	 * 
	 */
	private def queryTicket(ticketId: Long, f: Ticket => Result) = {
		val ticket = TicketStorage.get(ticketId)
				ticket match {
				case None => BadRequest(s"Could not find ticket id $ticketId")
				case _ 	=> f(ticket.get)
		}
	}
	
	def preflight(all: String) = Action {
		Ok("").withHeaders("Access-Control-Allow-Origin" -> "*",
				"Allow" -> "*",
				"Access-Control-Allow-Methods" -> "POST, GET, PUT, DELETE, OPTIONS",
				"Access-Control-Allow-Headers" -> "Origin, X-Requested-With, Content-Type, Accept, Referrer, User-Agent");
	}
}
