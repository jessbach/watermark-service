package service

trait Storageable[A, B] {
  
  var storage = List[A]()

  def identifier(item: A) : B
  
  def put(item :A) = storage = storage.::(item)

  def delete(key :B) = storage = storage.filter(item => identifier(item) != key)
  
  def get(key :B) : Option[A] = storage.find(item => identifier(item) == key)
  
}

