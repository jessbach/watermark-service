package service

import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global

import scala.util.Random
import play.api.libs.json._
import models.{Ticket, TicketStorage}

object WatermarkService extends EventSource[JsValue] {

	def generateWatermark(ticket: Ticket): Future[Ticket] = {
		import scala.concurrent.ExecutionContext.Implicits.global
		createWatermark(ticket)
	}

	private def createWatermark(ticket: Ticket) = Future[Ticket] {
		TicketStorage.put(ticket)
		notify(Json.toJson(ticket))
		
		val ticketInProgress = TicketStorage.invokeAndUpdate(ticket, t => t.process())

		// Simulate some work and to show the async mode
		val k = random(4, 9)
		val sleepTime = 1000.00
		val processDuration : Double = k * sleepTime;

		for (x <- 1 to k) {
			ticketInProgress.update(x * sleepTime / processDuration)
			notify(Json.toJson(ticketInProgress))
			Thread sleep 1000
		}

		// Document knows its watermarking itself, optionally can be modified
		ticketInProgress.document generateWatermark
		// doc.updateWatermark(Seq((WatermarkValue.Content, Content.Book.toString)))
		
		val ticketDone = TicketStorage.invokeAndUpdate(ticketInProgress, t => t.finish())
		notify(Json.toJson(ticketDone))
		
		ticketDone
	}

	private def random(min: Int, max: Int) : Int = {
		Random.nextInt(max - min) + min
	}

}