package models

import play.api.libs.functional.syntax._
import play.api.libs.json._
import _root_.util.EnumUtil
import models.Topic.Topic

case class Author(firstname: String, lastname: String) {
  require(firstname != null, "authors firstname must not be null")
  require(lastname != null, "authors firstname must not be null")
  override def toString: String = {
    s"$firstname $lastname"
  }
}

object Author {

	implicit val authorReads: Reads[Author] = (
		(JsPath \ "firstname").read[String] and
		(JsPath \ "lastname").read[String]
		)(Author.apply _)

		implicit val authorToJson: Writes[Author] = new Writes[Author] {
			override def writes(author: Author) = {
				Json.obj("firstname" -> author.firstname , "lastname" -> author.lastname)
			}
		}

		implicit def createAuthorFromString(s: String) : Author = {
			require(s != null, "author must not be null")
			val tokens = s.split(" ");
			if (tokens.length == 2) {
				Author(tokens(0), tokens(1))
			} else {
				throw new IllegalArgumentException("Cannot convert String %s to author!" format s);
			}
		}
}
