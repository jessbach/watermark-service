package models

import service.Storageable

object TicketStorage extends Storageable[Ticket, Long] {
  
  def identifier(item: Ticket) : Long = item.key
  
  def invokeAndUpdate(oldTicket: Ticket, f: Ticket => Ticket) : Ticket = {
    val idx = storage.indexWhere(_ == oldTicket)
    if (idx == -1) {
      throw new IllegalArgumentException(s"Ticket not in cache $oldTicket")
    } else {
      val newTicket = f(oldTicket)
      storage = storage.updated(idx, newTicket)
      newTicket
    }
  }
  
}