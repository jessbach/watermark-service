package models

import play.api.libs.json.{Reads, Json, Writes}
import util.EnumUtil
import scala.util.Random

case class Ticket(val document: Document, val status: Status.Value, val key: Long, var percentage: Double) {
	require(document != null, 	"document must not be null")
	require(status != null, 	"status must not be null")
	require(key > 0, 			"key must be greater than zero")
	require(percentage >= 0, 	"percentage must be greater or qual zero")
	
	def this(document: Document) = this(document, Status.Init, Random.nextInt(Integer.MAX_VALUE), 0.0)

	final def process(): Ticket = if (status == Status.Init) this.copy(status = Status.Processing) else throw new IllegalStateException(s"Cannot process Document. Status was [$status] instead of Init")
	
	final def update(p: Double): Unit = percentage = p
	
	final def finish(): Ticket = if (status == Status.Processing) this.copy(status = Status.Done, percentage = 1.0) else throw new IllegalStateException(s"Cannot finish Document. Status was [$status] instead of Processing")
}

object Ticket {
	implicit val ticketToJson: Writes[Ticket] = new Writes[Ticket] {
		override def writes(ticket: Ticket) = {
			Json.obj("id" -> ticket.key, "status" -> ticket.status.toString, "percentage" -> ticket.percentage.toString)
		}
	}
}

object Status extends Enumeration {
	type Status = Value
	val Init, Processing, Done = Value
}
