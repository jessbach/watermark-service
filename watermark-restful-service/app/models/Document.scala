package models

import play.api.libs.functional.syntax._
import play.api.libs.json._
import _root_.util.EnumUtil
import models.Topic.Topic

trait Watermarkable {
  type Watermark = Seq[(WatermarkValue.Value, String)]
  var watermark: Watermark = Nil
  def isWatermarked: Boolean = watermark != Nil
  def updateWatermark(w : Watermark) : Watermark = {
    if (!isWatermarked)
    	this.watermark = w
    watermark
  }
}

sealed abstract class Document(val title: String, val author: Author) extends Watermarkable {
  require(title != null, "title must not be null")
  require(author != null, "author must not be null")
  def generateWatermark : Watermark;
}

case class Book(bookTitle: String, bookAuthor: Author, val bookTopic: Topic = Topic.Business) extends Document(bookTitle, bookAuthor) {
  require(bookTopic != null, "title must not be null")
  override def generateWatermark : Watermark = {
    updateWatermark(Seq((WatermarkValue.Content, Content.Book.toString), (WatermarkValue.Title, title), (WatermarkValue.Author, author.toString)))  
  } 
}

case class Journal(journalTitle: String, journalAuthor: Author) extends Document(journalTitle, journalAuthor) {
  override def generateWatermark : Watermark = {
    updateWatermark(Seq((WatermarkValue.Content, Content.Book.toString), (WatermarkValue.Title, title), (WatermarkValue.Author, author.toString)))
  } 
}

object Document {
  
	implicit val documentReads: Reads[Document] = (
			(JsPath \ "title").read[String] and
			(JsPath \ "author").read[Author] and
			(JsPath \ "topic").readNullable[Topic])(Document.apply _)
  
	implicit val documentToJson: Writes[Document] = new Writes[Document] {
		override def writes(document: Document) = {
			var json: JsObject = JsObject(null)
			document match {
			  case b: Book => {
			    json = Json.obj("title" -> document.title, "author" -> Json.toJson(document.author), "topic" -> Json.toJson(b.bookTopic))
			  }
			  case j: Journal => {
			    json = Json.obj("title" -> document.title, "author" -> Json.toJson(document.author)) 
			  }
			}
			json ++ Json.obj("watermark" -> Json.toJson(document.watermark map { a => a._1.toString().toLowerCase() -> a._2 } toMap))
			json
		}
	}
    
	def apply(title: String, author: Author, topic: Option[Topic]): Document = {
		topic.getOrElse(None) match {
			case None 	=> Journal(title, author)
			case _ 		=> Book(title, author, topic.get)
		}
	}
  
}

object WatermarkValue extends Enumeration {
  type WatermarkValue = Value
  val Topic, Content, Author, Title = Value
}

object Topic extends Enumeration {
  type Topic = Value
  val Science, Business = Value

  implicit val enumReads: 	Reads[Topic] 	= EnumUtil.enumReads(Topic)
  implicit def enumWrites: 	Writes[Topic] 	= EnumUtil.enumWrites
}

object Content extends Enumeration {
  type Content = Value
  val Book, Journal = Value
  
  implicit val enumReads: 	Reads[Content] 	= EnumUtil.enumReads(Content)
  implicit def enumWrites: 	Writes[Content] = EnumUtil.enumWrites
}
