import sbt._
import Keys._
import play.PlayScala

object Build extends Build {

  val specs 		= "org.specs2" %% "specs2-core" % "2.4.14" % "test"
  val specsJunit 	= "org.specs2" %% "specs2-junit" % "2.4.14" % "test"
  val specsMock 	= "org.specs2" %% "specs2-mock" % "2.4.14" % "test"
  val check 		= "org.scalacheck" %% "scalacheck" % "1.12.1" % "test"
  val xml 			= "org.scala-lang.modules" %% "scala-xml" % "1.0.2"
  
  lazy val root = (project in file(".")).enablePlugins(PlayScala).settings(
    name := "watermark-restful-service",
    version := "1.0.0-SNAPSHOT",
    scalaVersion := "2.11.4",
    libraryDependencies ++= Seq(specs, specsJunit, specsMock, check, xml)
  )
  
}
