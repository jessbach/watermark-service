package models

import models._
import models.Status.Status
import org.junit.runner._
import org.specs2.matcher.OptionLikeCheckedMatcher
import org.specs2.mutable._
import org.specs2.runner._
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test._

class TicketSpec extends Specification {

   "A Ticket" should {
     "throw an IllegalArgumentException with null document" in {
       new Ticket(null) should throwA[IllegalArgumentException]
     }
     "throw an IllegalArgumentException with null status" in {
       new Ticket(document, null, 1, 0) should throwA[IllegalArgumentException]
     }
     "throw an IllegalArgumentException with key smaller zero" in {
       new Ticket(document, Status.Init, -3, 0) should throwA[IllegalArgumentException]
     }    
     "throw an IllegalArgumentException with percentage smaller equals zero" in {
       new Ticket(document, Status.Init, 4, -3) should throwA[IllegalArgumentException]
     }
     "return a valid instance for valid parameters" in {
       val t = ticket
       t mustNotEqual null
     }     
     "be in state init on creation" in {
       ticket.status should beEqualTo(Status.Init)
     }
     "be able to change state from Init to Processing" in {
       val t = ticket.process()
       t.status should beEqualTo(Status.Processing)
     }
     "be able to create a copy of the original ticket while chaning state" in {
       val t = ticket
       val ticketProcess = t.process
       (t eq ticketProcess) mustNotEqual (true)
     }
     "be able to change state from Processing to Finish" in {
       val t = ticket
       val processingTicket = ticket.process()
       val finishedTicket  = processingTicket.finish()
       finishedTicket.status should beEqualTo(Status.Done)
     }
     "note be able to change its state directly from Init to Finish" in {
        ticket.finish() must throwA[IllegalStateException]
     }
     "be serializable to JSON" in {
       val t = ticket
       val json = Json.toJson(t)
       (json \ "id").as[Long] should beEqualTo(t.key)
     }
   }
   
   private def ticket() = {
	   new Ticket(document)
   }
   
   private def document() = {
	   new Book("The Scala Programming Language", "Martin Odersky")
   }
   
   
}