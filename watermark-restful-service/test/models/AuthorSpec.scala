package models

import models._
import org.junit.runner._
import org.specs2.matcher.OptionLikeCheckedMatcher
import org.specs2.mutable._
import org.specs2.runner._
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test._

class AuthorSpec extends Specification {
  
  "An Author" should {
    
    "throw an IllegalArgumentException for firstname null" in {
      new Author(null, "") must throwA[IllegalArgumentException]
    }
    
    "throw an IllegalArgumentException for lastname null" in {
      new Author("", null) must throwA[IllegalArgumentException]
    }
    
    "return a valid author for right firstname, lastname" in {
      val author = new Author("Jan", "Essbach")
      author must not be null
      author.firstname mustEqual "Jan"
      author.lastname mustEqual "Essbach"
    }
    
    "return a valid author for string (implicit)" in {
      ("Jan Essbach":Author) mustEqual new Author("Jan", "Essbach")
    }
    
    "not return a valid author for invalid string (implicit)" in {
    	("Jan":Author) must throwA[IllegalArgumentException]
    }
        
  } 
  
  
}
