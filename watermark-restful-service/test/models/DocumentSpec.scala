package models

import models._
import models.Topic.Topic
import org.junit.runner._
import org.specs2.matcher.OptionLikeCheckedMatcher
import org.specs2.mutable._
import org.specs2.runner._
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test._

class DocumentSpec extends Specification {

   "A Document" should {
     "should throw an IllegalArgumentException if title is null" in {
    	 new Book(null, "Jan Essbach") should throwA[IllegalArgumentException]
     }
     "should throw an IllegalArgumentException if author is null" in {
    	 new Book("Suprising Scala", null) should throwA[IllegalArgumentException]
     }
   }
   
   "A Book" should {
    "be convertable to json including all relevant fields" in {
      val document = new Book("Suprising Scala", "Jan Essbach", Topic.Science)
      
      val jsonDocument = Json.toJson(document)
      (jsonDocument \ "author").as[Author] must be equalTo new Author("Jan", "Essbach")
      (jsonDocument \ "title").as[String] must be equalTo "Suprising Scala"
      (jsonDocument \ "topic").as[Topic] must be equalTo Topic.Science

      jsonDocument should not beNull
    }

    "be deserializable from its json representation and represent the same book" in {
      val document = Book("Suprising Scala", "Jan Essbach", Topic.Science)
      val jsonDocument = Json.toJson(document)
      val deserialized = Json.fromJson[Document](jsonDocument).get
      
      deserialized should beEqualTo(document)
    }
  }

  "A Journal" should {
    "be convertable to json including all relevant fields" in {
      val document = Journal("Suprising Scala", "Jan Essbach")
      val jsonDocument = Json.toJson(document)
      (jsonDocument \ "author").as[Author] must be equalTo new Author("Jan", "Essbach")
      (jsonDocument \ "title").as[String] must be equalTo "Suprising Scala"
      jsonDocument should not beNull
    }

    "be deserializable from its Json representation and represent the same book" in {
      val document = Journal("Suprising Scala", "Jan Essbach")
      val jsonDocument = Json.toJson(document)
      val deserialized = Json.fromJson[Document](jsonDocument).get
      deserialized should beEqualTo(document)
    }
  }
  
  
}