package service

import models._
import org.junit.runner._
import org.specs2.matcher.OptionLikeCheckedMatcher
import org.specs2.mutable._
import org.specs2.runner._
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test._

class NotificationSpec extends Specification with EventSource[String] {

  "A NotificationService" should {
    
    "inform its observers about changing state" in {
       var changed = false
       listen(new Event[String] {
    	  def event(ev: String) {
    	    changed = true
    	  }
    	})
    	notify("Changed")
    	changed mustEqual true
     }
     "should not inform observer when added and then got removed" in {
       var changed = false
       val listener = new Event[String] {
    	  def event(ev: String) {
    	    changed = true
    	  }
       }
       remove(listen(listener))
       notify("Changed")
       changed mustEqual false
     }
     
  }
  
}