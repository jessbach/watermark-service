package service

import models._
import org.junit.runner._
import org.specs2.matcher.OptionLikeCheckedMatcher
import org.specs2.mutable._
import org.specs2.runner._
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test._
import service._

class StorageSpec extends Specification {

  "The DocumentStorage" should {
    "obtain nothing when a ticket for given id does not exist" in { 
      TicketStorage.get(42) should beNone
    }

    "return a ticket with state Init when it exists" in {
      val newTicket = createTicket()
      TicketStorage.put(newTicket)
      val retrievedTicket = TicketStorage.get(newTicket.key)

      retrievedTicket should beSome[Ticket]
      retrievedTicket.get.status should be equalTo Status.Init
      retrievedTicket.get.key should be equalTo newTicket.key
    }

    "be able to delete a stored element" in {
      val newTicket = createTicket()
      TicketStorage.put(newTicket)
      val retrievedTicket = TicketStorage.get(newTicket.key )
      val key = retrievedTicket.get.key
      TicketStorage.get(key) should beSome[Ticket]
      TicketStorage.delete(key)
      TicketStorage.get(key) should beNone
    }

    "be update a ticket and return the changed one" in {
      val newTicket = createTicket()
      TicketStorage.put(newTicket)
      val updatedTicket = TicketStorage.invokeAndUpdate(newTicket, t => t.process())
      updatedTicket.key should be equalTo newTicket.key
      updatedTicket.status should be equalTo Status.Processing
    }

    "be update a ticket and store the changed one" in {
      val newTicket = createTicket()
      TicketStorage.put(newTicket)
      val updatedTicket = TicketStorage.invokeAndUpdate(newTicket, t => t.process())
      val retrievedUpdatedTicket = TicketStorage.get(updatedTicket.key)
      updatedTicket should be equalTo retrievedUpdatedTicket.get
    }

    "be throw an IllegalargumentException for unpersistent tickets" in {
      val unpersistendTicket = createTicket()
       TicketStorage.invokeAndUpdate(unpersistendTicket, t => t.process())  must throwA[IllegalArgumentException]
    }
  }
  
  def createTicket() = new Ticket(new Book("Suprising Scala", "Jan Essbach", Topic.Science))
  
}