package controllers

import models._
import org.junit.runner._
import org.specs2.matcher.OptionLikeCheckedMatcher
import org.specs2.mutable._
import org.specs2.runner._
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test._

class ApplicationSpec extends Specification {

	"Restful-Service" should {

		// @POST
		"return ticket for a valid document post" in new WithApplication {
			val response = route(FakeRequest(POST, "/document").withJsonBody(Json.toJson(document))).get
			status(response) must equalTo(OK)
			contentType(response) must beSome.which(_ == "application/json")
			val jsonContent = contentAsJson(response)
			val jsonTicket = jsonContent \ "ticket"
			(jsonTicket \ "status").as[String] must beEqualTo("Init")
			(jsonTicket \ "id").as[Long] must not beNull
		}
		
		"return no ticket for an invalid document post" in new WithApplication {
			val response = route(FakeRequest(POST, "/document").withJsonBody(Json.parse("{\"answer\": \"42\"}"))).get
			status(response) must equalTo(BAD_REQUEST)
			contentType(response) must beJson
			val jsonContent = contentAsJson(response)
			(jsonContent \ "status").as[String] mustEqual ("error")
		}

		// @GET
		"return an ticket for valid id" in new WithApplication {
			val ticketId: Long = uploadDocumentAndObtainTicket
			val doc = document
			
			// Need to wait, generating watermark takes some time.. between 4 and 8 seconds
			Thread sleep (1000 * 10) 
			  
		  	val responseWithValidTicketId = route(FakeRequest(GET, s"/document/$ticketId")).get
		  	status(responseWithValidTicketId) must equalTo(OK)
		  	contentType(responseWithValidTicketId) must beJson
		}
		
		"return an bad request for an invalid ticket id" in new WithApplication {
			val responseInvalidTicketId = route(FakeRequest(GET, "/document/123")).get
			status(responseInvalidTicketId) must equalTo(BAD_REQUEST)
			contentAsString(responseInvalidTicketId) must equalTo("Could not find ticket id 123")
		}
				
		// @DELETE
	    "return bad request for an invalid ticket to delete" in new WithApplication {
	      val responseInvalidTicketId = route(FakeRequest(DELETE, "/document/123")).get
	      status(responseInvalidTicketId) must equalTo(BAD_REQUEST)
	      contentType(responseInvalidTicketId) must beSome.which(_ == "text/plain")
	      contentAsString(responseInvalidTicketId) must equalTo("Could not find ticket id 123")
	    }
	    
	    "be able to delete a valid ticket" in new WithApplication {
	      val id = uploadDocumentAndObtainTicket()
	      val responseValidTicketId = route(FakeRequest(DELETE, s"/document/$id")).get
	      status(responseValidTicketId) must equalTo(OK)
	      contentType(responseValidTicketId) must beSome.which(_ == "text/plain")
	      contentAsString(responseValidTicketId) must endWith(" deleted")
	    }

	    // @HEAD
	    "be able to check with a valid ticket" in new WithApplication {
	      val id = uploadDocumentAndObtainTicket()
	      val responseValidTicketId = route(FakeRequest(HEAD, s"/ticket/$id")).get
	      status(responseValidTicketId) must equalTo(OK)
	      contentType(responseValidTicketId) must beSome.which(_ == "text/plain")
	    }
	
	    "return bad request for an invalid ticket" in new WithApplication {
	      val responseInvalidTicketId = route(FakeRequest(HEAD, "/ticket/123")).get
	      status(responseInvalidTicketId) must equalTo(BAD_REQUEST)
	      contentType(responseInvalidTicketId) must beSome.which(_ == "text/plain")
	      contentAsString(responseInvalidTicketId) must equalTo("Could not find ticket id 123")
	    }
	    
	}

	private def uploadDocumentAndObtainTicket() : Long = {
		val response 	= route(FakeRequest(POST, "/document").withJsonBody(Json.toJson(document))).get
		val jsonContent = contentAsJson(response)
		val jsonTicket 	= jsonContent \ "ticket"
		(jsonTicket \ "id").as[Long]
	}

	private def document() = {
		new Book("The Scala Programming Language", "Martin Odersky")
	}

	private def beJson: OptionLikeCheckedMatcher[Option, String, String] = {
		beSome.which(_ == "application/json")
	}

}