Springer Watermark Test 
------------------
This project realises an asynchronous watermarking service for documents such as books or journals. The service is realized as scala play web service, supporting a rest interface on the server side and two client implementations using ajax polling and websocket bidirectional communication.

Run
------------------
The server application can be hit with 

	> sbt run

Clients are having fixed resource connection to http://127.0.0.1:9000, maybe need to be changed.

Test
------------------
The test feature can be hit with 

    > sbt test

Requires
------------------
* Please download latest version of SBT.
* [sbt 0.13.7](http://www.scala-sbt.org)
* Scala 			2.11.4
* sbt 			0.13.7
* Specs 2 		2.4.14
* sbt-scoverage 	1.0.1
* PlayFramework
* jQuery
* Twitter Bootstrap
* Socket.io

Play Framework Issues
--------------------
https://github.com/fdimuccio/play2-sockjs/issues/11

Author
--------------------
Jan Essbach

[@janessabch](http://twitter.com/janessbach)